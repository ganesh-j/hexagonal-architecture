package com.classpath.paybiz;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PayBizApplication {

  public static void main(String[] args) {
    SpringApplication.run(PayBizApplication.class, args);
  }

}
