package com.classpath.paybiz.account.application.service;

import com.classpath.paybiz.account.domain.Account;
import com.classpath.paybiz.account.application.port.out.AccountLock;
import org.springframework.stereotype.Component;

@Component
class NoOpAccountLock implements AccountLock {

	@Override
	public void lockAccount(Account.AccountId accountId) {
		// do nothing
	}

	@Override
	public void releaseAccount(Account.AccountId accountId) {
		// do nothing
	}

}
